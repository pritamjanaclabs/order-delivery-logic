/**
 * Created by karan on 3/9/2015.
 */
var fs = require('fs');
var AWS = require('aws-sdk');
var sendResponse = require('./sendResponse');
var request = require('request');
var awsConfig = config.get('awsSettings');
var nodeMailer = require("nodemailer");
var mailConfig=config.get('emailSettings');

//definition of checkBlank that calling from user.js file
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}


//check passing parameter is blank or not
function checkBlank(arr) {

    var arrLength = arr.length;

    for (var i = 0; i < arrLength; i++) {
        if ((arr[i] == '')||(arr[i] == undefined)||(arr[i] == '(null)')) {
            return 1;
            break;
        }
    }
    return 0;
}

//check the request mail id already registered or not.
exports.checkEmailAvailability=function(res,email,callback){
    var sql = "SELECT `id` FROM `tb_user` WHERE `email`=? limit 1";
    var values = [email];

    dbConnection.Query(res, sql, values, function (userResponse) {

        if (userResponse.length) {
            // var errorMsg = 'Email already exists!';
            sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_EXISTS, res);
        }
        else {
            //res.send("Okay");
            callback();
        }
    });
}

//check email id is Exist On SMTP Server or not
exports.checkValidMailOnSMTP=function (ress,userMail,callback){
    request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: userMail},
        json: true
    }, function (err,res, body) {
        //console.log(body.valid);
        if (body.valid) {
            callback (null);
        }
        else {
            sendResponse.wrongMailId(ress);
            console.log(userMail)
            console.log("Email not Exist On SMTP Server");
        }
    });
}

//image upload on s3 bucket with access key and also secretAccessKey
exports.imageUploadOnServer=function(img,folder,callback){

    var filename = img.name; // actual filename of file
    var path = img.path; //will be put into a temp directory
    var mimeType = img.type;
    /*console.log(filename);
     console.log(path);
     console.log(extention);*/

    fs.readFile(path, function (err, file_buffer) {
        if(err){
            return callback(0);
        }
        else{
            AWS.config.update({accessKeyId: awsConfig.awsAccessKey, secretAccessKey: awsConfig.awsSecretAccessKey});
            var s3bucket = new AWS.S3();
            var params = {Bucket: awsConfig.awsUserName, Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mimeType};

            s3bucket.putObject(params, function(err, data) {
                if (err)
                {
                    console.log(err)
                    return callback(0);
                }
                else{
                    var url= awsConfig.awsURL+folder+'/'+filename
                    //console.log(url);
                    return callback(url);
                }
            });
        }

    });
};
//send mail to mail id with nodemailler module
exports.sendEmail = function(receiverMailId, subject,message, callback) {
    var smtpTransport = nodeMailer.createTransport("SMTP", {
        service: 'Gmail',
        auth: {
            user: mailConfig.email,
            pass: mailConfig.password
        }
    });

    var mailOptions = {
        from: mailConfig.email,
        to: receiverMailId,
        subject: subject,
        text: 'WellCome To ------',
        html: '<b>'+message+'</b>'

    }
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            return callback(0);
        } else {
            //  console.log('hi');
            return callback(1);
        }
    });
};


// this function is used to sort a json object
exports.sortByKey=function (array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

