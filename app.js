process.env.NODE_ENV = 'test';
config = require('config');
constant = require('./routes/constant');
dbConnection = require('./routes/dbConnection');

var express = require('express');
var path = require('path');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var bodyParser = require('body-parser');

var http = require('http');

var routes = require('./routes/index');
var order = require('./routes/setOrder');
var user = require('./routes/user');
var driver = require('./routes/driver');

var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.get('/', routes);
app.post('/end_service', order);
app.post('/start_service', order);
app.post('/confirm_order', order);
app.post('/set_order', order);
app.post('/get_driver', order);
app.post('/add_user', multipartMiddleware);
app.post('/add_user', user);
app.post('/add_driver', multipartMiddleware);
app.post('/add_driver', driver);
app.post('/user_login',user);
app.post('/logout',user);
app.post('/forgot_password',user);
app.get('/reset_password', user);
app.post('/setPassword', user);
app.post('/edit_user_profile', multipartMiddleware);
app.post('/edit_user_profile',user);
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});