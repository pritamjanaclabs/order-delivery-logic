/**
 * Created by karan on 3/9/2015.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

exports.responseStatus = {};
define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 101);
define(exports.responseStatus, "SHOW_MESSAGE", 102);
define(exports.responseStatus, "SHOW_DATA", 103);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN", 104);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "SHOW_MESSAGE", "Successfully Registered!");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "UPLOADING_ERROR", "Image uploading error please try again");
define(exports.responseMessage, "DB_INSERTING_ERROR", "Image inserting error in database please try again");
define(exports.responseMessage, "UNAUTHORISED_LOGIN", "Invalid password");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN",'Invalid Access Token');
define(exports.responseMessage, "LOGOUT_SUCCESSFUL",'Successfully Logout');
define(exports.responseMessage, "WAIT_FOR_VERIFICATION",'Please wait for a verification by admin.');
define(exports.responseMessage, "SENT_EMAIL",'Mail has been sent to reset your password');
define(exports.responseMessage, "MAIL_SENDING_ERROR",'Sending mail error please try again');
define(exports.responseMessage, "SUCCESSFULLY_UPDATE", 'Successfully updated');
define(exports.responseMessage, "LINK_EXPIRED", 'Link has been expired');
define(exports.responseMessage, "DRIVER_NOT_AVAILABLE", 'Driver not available');
define(exports.responseMessage, "ORDER_ALREADY_ASSIGNED", 'Sorry, This order already assigned');
define(exports.responseMessage, "ALREADY_DELETED_REQUEST", 'Oops! Hit Request Already Deleted');
define(exports.responseMessage, "ORDERED_NOT_CONFIRMED", 'Sorry no any confirmed ordered');
define(exports.responseMessage, "ORDERED_NOT_ASSIGNED", 'Sorry you have no any order');
define(exports.responseMessage, "ORDERED_NOT_ACCEPTED", 'Sorry you have no any accept order');
define(exports.responseMessage, "ALREADY_SETUP_RIDING", 'Oops! You already setup a riding');
define(exports.responseMessage, "WRONG_IN_ORDER_TB", 'Client ID not found!!');
define(exports.responseMessage, "NO_CLIENT_AVAILABLE", 'No any client according to this ordered');
