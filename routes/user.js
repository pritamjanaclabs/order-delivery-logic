/**
 * Created by karan on 3/13/2015.
 */
var express = require('express');
var router = express.Router();
var commonFunc = require('./commonFunction');
var async = require('async');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');

router.post('/add_user', function (req, res) {
    var userName = req.body.user_name;
    var userEmail=req.body.user_email;
    var userPhone=req.body.user_phone_no;
    var userPassword=req.body.user_password;
    var userDeviceToken=req.body.device_token;
    var userDeviceType=req.body.device_type;
    var userAddress=req.body.user_address;
    var userCurrentLatitude = req.body.drop_latitude;
    var userCurrentLongitude = req.body.drop_longitude;
    var userImage=req.files.user_image;
    var checkValues = [userName,userEmail,userPhone,userPassword,userDeviceToken,userDeviceType,userAddress, userCurrentLatitude, userCurrentLongitude,userImage.name];
    var currentTime = new Date().getTime().toString();
    req.files.user_image.name = currentTime;
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkValues, callback);
        },function(callback){
            commonFunc.checkEmailAvailability(res, userEmail, callback);
        },function (callback){
            commonFunc.checkValidMailOnSMTP(res,userEmail,callback);
        }], function (resultCallback) {
            commonFunc.imageUploadOnServer(req.files.user_image, 'pjImages', function (imgLink) {
                if (imgLink === 0) {
                    console.log("error");
                    sendResponse.sendErrorMessage(constant.responseMessage.UPLOADING_ERROR, res);
                }
                else {
                    var userImage=imgLink;
                    var loginTime = new Date();
                    var accessToken = md5(userEmail + userPassword + loginTime);
                    var encryptPassword = md5(userPassword);
                    var appVersion=100;     //initial app version
                    var regAs=0;            //0: customer, 1: both
                    var status=2;           //0: offline, 1: online as a driver, 2: online as a customer
                    var verified=0;         //0: not, 1: yes
                    var deals=1            //0: assigned, 1: idle, 5: request, 6: accept,
                    var oneTimeLink=0      //initially 0
                    var sql = "INSERT INTO `tb_user`(`name`,`email`,`phone_no`,`password`,`address`, `current_latitude`, `current_longitude`,`date_registered`,`last_login`,`user_image`,`device_token`,`access_token`,`device_type`,`app_version`, `reg_as`,`status`,`verified`,`deals`,`one_time_link`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    var values = [userName, userEmail,userPhone,encryptPassword,userAddress, userCurrentLatitude, userCurrentLongitude,loginTime,loginTime,userImage,userDeviceToken,accessToken,userDeviceType,appVersion, regAs,status,verified,deals,oneTimeLink];
                    dbConnection.Query(res, sql, values, function (userInsertResult) {

                        var data = {"user_name": userName, "user_image": userImage, "access_token": accessToken,
                            "email": userEmail, "phone_no": userPhone};
                        sendResponse.sendSuccessData(data, res);
                    });
                }
            });
        //res.send("valid");
    });
    //console.log(req.body.user_name);
    //res.send(req.body.user_name);
});

//user_login for user and driver also
router.post('/user_login',function(req,res){
    var userEmail=req.body.user_email;
    var password = req.body.user_password;
    var userCurrentLatitude = req.body.Current_latitude;
    var userCurrentLongitude = req.body.Current_longitude;
    var deviceType = req.body.device_type; // 0 for android, 1 for ios, 2 for windows
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var checkValues = [userEmail,password,userCurrentLatitude,userCurrentLongitude,deviceType,deviceToken,appVersion];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkValues, callback);
        }],function(resultCallback){
        var encryptPassword = md5(password);
        var sql = "SELECT `id` FROM `tb_user` WHERE `email`=? limit 1";
        dbConnection.Query(res,sql, [userEmail] , function (userResponse) {
            if (userResponse.length == 0) {
                sendResponse.wrongMailId(res);
            }
            else {
                var userId=userResponse[0].id;
                var sql = "SELECT `reg_as`,`verified` FROM `tb_user` WHERE `id`=? AND `password`=? limit 1";
                dbConnection.Query(res,sql, [userId,encryptPassword] , function (userResponse2) {
                    if (userResponse2.length == 0) {
                        sendResponse.sendErrorMessage(constant.responseMessage.UNAUTHORISED_LOGIN, res);
                    }
                    else {
                        var regAs=userResponse2[0].reg_as;
                        var checkVerified=userResponse2[0].verified;
                        if(checkVerified==0 && regAs==1){
                            sendResponse.sendErrorMessage(constant.responseMessage.WAIT_FOR_VERIFICATION, res);
                        }
                        else{
                            var loginTime = new Date();
                            var accessToken = md5(userEmail + password + loginTime);
                            var onlineStatus=0;
                            if(regAs==0){
                                onlineStatus=2;
                            }
                            else{
                                onlineStatus=1;
                            }

                            //console.log(loginTime);
                            var sql = "UPDATE `tb_user` set `access_token`=?, `status`=?,`current_latitude`=?,`current_longitude`=?,`last_login`=? WHERE id=?";
                            dbConnection.Query(res,sql, [accessToken,onlineStatus,userCurrentLatitude,userCurrentLongitude,loginTime,userId] , function (userResponse3){
                                var sql = "SELECT `name`,`phone_no`,`user_image`,`access_token`,`deals` FROM `tb_user` WHERE `id`=? limit 1";
                                dbConnection.Query(res,sql, [userId] , function (userResponse4) {
                                    var dealingStage=userResponse4[0].deals;
                                    var dealingStageMsg='';
                                    switch (dealingStage){
                                        case 0:
                                            dealingStageMsg='assigned';
                                            break;
                                        case 5:
                                            dealingStageMsg='request';
                                            break;
                                        case 6:
                                            dealingStageMsg='accept';
                                            break;
                                        default :
                                            dealingStageMsg='idle';
                                            break;
                                    }
                                    var data={
                                        Name:userResponse4[0].name,
                                        PhoneNo:userResponse4[0].phone_no,
                                        Image:userResponse4[0].user_image,
                                        AccessToken:userResponse4[0].access_token,
                                        DealingStage:dealingStageMsg,
                                        DealingCode:dealingStage
                                    };
                                    sendResponse.sendSuccessData(data, res);
                                });
                            });
                        }
                    }
                });
            }
        });
    });
});

//logout for user as well as driver
router.post('/logout',function(req,res){
    var accessToken=req.body.access_token;
    var sql = "UPDATE `tb_user` set `status`=? where `access_token`=?";
    dbConnection.Query(res,sql, [0,accessToken] , function (userResponse2) {
        if(userResponse2.affectedRows==0) {
            sendResponse.sendErrorMessage(constant.responseMessage.INVALID_ACCESS_TOKEN, res);
        }
        else {
            sendResponse.sendSuccessData(constant.responseMessage.LOGOUT_SUCCESSFUL, res);
        }
    });
});


//forgot password for user and also driver
router.post('/forgot_password',function(req,res){
    var email = req.body.email;
    var values = [email];
    async.waterfall([
            function (callback) {
                commonFunc.checkBlank(res, values, callback);
            },function (callback) {
                checkExist(res,email,callback);
            }],function(resultCallback){
            var sub ="Reset Password";
            var port=config.get('PORT');
            var message ='<a href="http://localhost:'+port+'/reset_password?token='+resultCallback+'">Reset your password</a>';
            commonFunc.sendEmail(email, sub, message, function (result) {
                if (result == 1) {
                    sendResponse.sendSuccessData(constant.responseMessage.SENT_EMAIL,res);
                }
                else{
                    sendResponse.sendErrorMessage(constant.responseMessage.MAIL_SENDING_ERROR,res);
                }
            });
        }
    );
});

//get reset password from mail id
router.get('/reset_password', function(req, res, next) {
    res.sendfile('./views/resetPassword.html');
});

//check access token and also change password
router.post('/setPassword', function(req, res, next) {
    var oneTimeLink = req.body.oneTimeLink;
    var newPassword = req.body.password;
    var values = [oneTimeLink, newPassword];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, values, callback);
        }], function (callbackResult) {
        var sql = "SELECT `id`,`email` FROM `tb_user` WHERE `one_time_link`=? LIMIT 1";
        dbConnection.Query(res, sql, oneTimeLink, function (result) {
            if (result.length == 0) {
                sendResponse.sendErrorMessage(constant.responseMessage.LINK_EXPIRED,res);
            }
            else {
                var userId = result[0].id;
                var userMail = result[0].email;
                var encryptPassword = md5(newPassword);
                var loginTime = new Date();
                var newAccessToken = md5(userMail + newPassword + loginTime);
                var sql = "UPDATE `tb_user` SET `password`=?,`access_token`=?,`one_time_link`=? WHERE `id`=? LIMIT 1";
                dbConnection.Query(res, sql, [encryptPassword, newAccessToken,0, userId], function (userResponse2) {
                    sendResponse.sendSuccessData(constant.responseMessage.SUCCESSFULLY_UPDATE, res);
                });

            }
        });

    });

});

//check mail id is it exsit or not
function checkExist(res,email,callback) {
    var sql = "SELECT `id`,`name`,`access_token`,`password` FROM `tb_user` WHERE `email`=? limit 1";
    dbConnection.Query(res, sql, email, function (userResponse) {
        if (userResponse.length == 0) {
            sendResponse.wrongMailId(res);
        }
        else{
            //return callback(userResponse);
            var userId=userResponse[0].id;
            var loginTime = new Date();
            var oneTimeLink=md5(email+userResponse[0].access_token+loginTime);
            var sql = "UPDATE `tb_user` SET `one_time_link`=? WHERE `id`=? LIMIT 1";
            dbConnection.Query(res, sql, [oneTimeLink, userId], function (userResponse2) {
                return callback(oneTimeLink);
            });
        }
    });
}

//edit profile of user as well as driver
router.post('/edit_user_profile',function(req,res){
    var accessToken=req.body.access_token;
    var userName = req.body.user_name;
    var userPhone=req.body.user_phone_no;
    var userAddress=req.body.user_address;
    var userImage=req.files.user_image;

    var checkValues = [accessToken,userName,userPhone,userAddress,userImage.name];
    var currentTime = new Date().getTime().toString();
    req.files.user_image.name = currentTime;
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkValues, callback);
        }],function(resultCallback){
        var sql="SELECT `id` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
        dbConnection.Query(res, sql, [accessToken], function (userResponse) {
            if (userResponse.length == 0) {
                sendResponse.sendErrorMessage(constant.responseMessage.INVALID_ACCESS_TOKEN, res);
            }
            else{
                var userId=userResponse[0].id;
                commonFunc.imageUploadOnServer(req.files.user_image, 'pjImages', function (imgLink) {
                    if (imgLink === 0) {
                        console.log("error");
                        sendResponse.sendErrorMessage(constant.responseMessage.UPLOADING_ERROR, res);
                    }
                    else {
                        var imageLink=imgLink;
                        var sql = "UPDATE `tb_user` SET `name`=?,`phone_no`=?,`address`=?,`user_image`=? WHERE `id`=? LIMIT 1";
                        dbConnection.Query(res, sql, [userName,userPhone,userAddress,imageLink, userId], function (userResponse2) {
                            var sql = "SELECT `name`,`phone_no`,`user_image`,`access_token` FROM `tb_user` WHERE `id`=? limit 1";
                            dbConnection.Query(res,sql, [userId] , function (userResponse4) {
                                var data={
                                    Name:userResponse4[0].name,
                                    PhoneNo:userResponse4[0].phone_no,
                                    Image:userResponse4[0].user_image,
                                    AccessToken:userResponse4[0].access_token
                                };
                                sendResponse.sendSuccessData(data, res);
                            });
                        });
                    }
                });
            }
        });
    });
});

module.exports = router;