/**
 * Created by karan on 3/12/2015.
 */
var express = require('express');
var router = express.Router();
var commonFunc = require('./commonFunction');
var async = require('async');
var sendResponse = require('./sendResponse');
var geoLib = require('geolib');

/* set order api use to book a driver who is available. now*/
router.post('/set_order',function(req,res){
    var userAccessToken = req.body.access_token;
    var userLatitude = req.body.latitude;
    var userLongitude = req.body.longitude;
    var radiusFromUser = req.body.km;
    var pickUpAddress=req.body.pickupAddress;
    var checkVal = [userAccessToken, userLatitude, userLongitude, radiusFromUser,pickUpAddress];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkVal, callback);
        }, function (callback) {
            getDrivers(res, userAccessToken, userLatitude, userLongitude, radiusFromUser, callback);
        }], function (resultCallback) {
        var sql = "SELECT `id` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
        dbConnection.Query(res, sql, [userAccessToken], function (result1) {
            if (result1.length == 0) {
                sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN, constant.responseMessage.INVALID_ACCESS_TOKEN, res);
            }
            else {
                var userId=result1[0].id;
                var sql = "SELECT `deals` FROM `tb_user` WHERE `id`=? LIMIT 1";
                dbConnection.Query(res, sql, [userId], function (result111) {
                    if (result111.length == 0) {
                        sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN, constant.responseMessage.INVALID_ACCESS_TOKEN, res);
                    }
                    else {
                        var userDeals=result111[0].deals;
                        if(userDeals==1){
                            if(resultCallback.length>0){
                                var dateOfOrdered = new Date();
                                var sql = "INSERT INTO `tb_order`(`customer_id`,`status`,`Order_date_time`,`pickup_location_address`) VALUES (?,?,?,?)";
                                var values = [userId, 0,dateOfOrdered,pickUpAddress];
                                dbConnection.Query(res, sql, values, function (userInsertResult) {
                                    //console.log(userInsertResult.insertId);
                                    var orderId=userInsertResult.insertId;
                                    for(var i=0;i<resultCallback.length;i++){
                                        var driverId=resultCallback[i].id;
                                        var sql = "UPDATE `tb_user` set `deals`=?,`last_order_id`=? WHERE `id`=? AND `deals`=?";
                                        dbConnection.Query(res,sql, [5,orderId,driverId,1] , function (userResponse3){
                                        });
                                    }
                                    var sql = "UPDATE `tb_user` set `deals`=?,`last_order_id`=? WHERE `id`=?";
                                    dbConnection.Query(res,sql, [0,orderId,userId] , function (userResponse3){
                                    });
                                    //send notification to user and also driver for set order
                                    var data={
                                        order_id:orderId,
                                        message:'Please wait for confirmation from driver'
                                    };
                                    sendResponse.sendSuccessData(data, res);
                                });
                            }
                            else{
                                sendResponse.sendErrorMessage(constant.responseMessage.DRIVER_NOT_AVAILABLE, res);
                            }
                        }
                        else{
                            sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.ALREADY_SETUP_RIDING, res);
                        }
                    }
                });
            }
        });
    });
});

//confirm_order API is used to confirm a order request by driver
router.post('/confirm_order',function(req,res){
    var driverAccessToken = req.body.access_token;
    var checkVal=[driverAccessToken];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkVal, callback);
        }],function(resultCallback){
        var sql = "SELECT `id` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
        dbConnection.Query(res, sql, [driverAccessToken], function (result1) {
            if (result1.length == 0) {
                sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN, constant.responseMessage.INVALID_ACCESS_TOKEN, res);
            }
            else {
                var driverId = result1[0].id;
                var sql = "SELECT `last_order_id` FROM `tb_user` WHERE `id`=? AND `deals`=? LIMIT 1";
                dbConnection.Query(res, sql, [driverId,5], function (result11) {
                    if (result11.length == 0) {
                        sendResponse.sendErrorMessage(constant.responseMessage.ORDER_ALREADY_ASSIGNED, res);
                    }
                    else {
                        var lastOrderIdOfDriver=result11[0].last_order_id;
                        var sql="UPDATE `tb_user` SET `deals`=? WHERE `id`=?";
                        dbConnection.Query(res,sql, [6,driverId] , function (userResponse2){
                            var sql="UPDATE `tb_user` SET `deals`=?,`last_order_id`=? WHERE `last_order_id`=? AND `deals`=?";
                            dbConnection.Query(res,sql, [1,0,lastOrderIdOfDriver,5] , function (userResponse3){
                                var sql="UPDATE `tb_order` SET `driver_id`=?,`status`=? WHERE `id`=? ";
                                dbConnection.Query(res,sql, [driverId,1,lastOrderIdOfDriver] , function (userResponse3){
                                    var sql = "SELECT `customer_id`,`pickup_location_address` FROM `tb_order` WHERE `id`=? LIMIT 1";
                                    dbConnection.Query(res, sql, [lastOrderIdOfDriver], function (result4) {
                                        if (result4.length == 0) {
                                            sendResponse.sendErrorMessage(constant.responseMessage.ALREADY_DELETED_REQUEST, res);
                                        }
                                        else {
                                            //send notification to user and also driver for confirmation
                                            var customerId=result4[0].id;
                                            var data={
                                                customer_id:customerId,
                                                pickup_location:result4[0].pickup_location_address
                                            };
                                            sendResponse.sendSuccessData(data, res);
                                        }
                                    })
                                });
                            });
                        });
                    }
                })
            }
        });
    });
});

//Start service API is used to start service by driver according to order
router.post('/start_service',function(req,res){
    var driverAccessToken = req.body.access_token;
    var pickupLatitude = req.body.latitude;
    var pickupLongitude = req.body.longitude;
    var checkVal=[driverAccessToken,pickupLatitude,pickupLongitude];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkVal, callback);
        }],function(resultCallback) {
        var sql = "SELECT `id`,`last_order_id`,`deals` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
        dbConnection.Query(res, sql, [driverAccessToken], function (result1) {
            if (result1.length == 0) {
                sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN, constant.responseMessage.INVALID_ACCESS_TOKEN, res);
            }
            else {
                var driverId=result1[0].id;
                var orderIdOfDriver=result1[0].last_order_id;
                var checkAccepted=result1[0].deals;
                if(orderIdOfDriver!=0){
                    if(checkAccepted==6){
                        var sql = "SELECT `customer_id`,`pickup_location_address` FROM `tb_order` WHERE `id`=? AND `status`=? AND `driver_id`=? LIMIT 1";
                        dbConnection.Query(res, sql, [orderIdOfDriver,1,driverId], function (result2) {
                            if (result2.length == 0) {
                                sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.ORDERED_NOT_CONFIRMED, res);
                            }
                            else {
                                var customerId=result2[0].customer_id;
                                var pickUpAddress=result2[0].pickup_location_address;
                                var dateOfPickup = new Date();
                                var sql="UPDATE `tb_order` SET `status`=?,`pickup_time`=?,`pickup_latitude`=?,`pickup_longitude`=? WHERE `id`=? ";
                                dbConnection.Query(res,sql, [2,dateOfPickup,pickupLatitude,pickupLongitude,orderIdOfDriver] , function (userResponse3){

                                    //send notification to user and also driver for start service
                                    var data={
                                        customerId:customerId,
                                        pickupLocation:pickUpAddress,
                                        pickupTime:dateOfPickup
                                    };
                                    sendResponse.sendSuccessData(data, res);
                                });
                            }
                        });
                    }
                    else{
                        sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.ORDERED_NOT_CONFIRMED, res);
                    }
                }
                else{
                    sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.ORDERED_ONT_ASSIGNED, res);
                }
            }
        });
        //res.send('gfhfgh');
    });
});

//end service is used to get a result at complete the raiding.
router.post('/end_service',function(req,res){
    var driverAccessToken = req.body.access_token;
    var deliveredLatitude = req.body.latitude;
    var deliveredLongitude = req.body.longitude;
    var deliveredPrice = req.body.price;
    var deliveredAddress=req.body.delivered_address;

    var checkVal=[driverAccessToken,deliveredLatitude,deliveredLongitude,deliveredPrice,deliveredAddress];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkVal, callback);
        }],function(resultCallback) {
        var sql = "SELECT `id`,`last_order_id`,`deals`,`total_deals` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
        dbConnection.Query(res, sql, [driverAccessToken], function (result1) {
            if (result1.length == 0) {
                sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN, constant.responseMessage.INVALID_ACCESS_TOKEN, res);
            }
            else {
                var driverId=result1[0].id;
                var driverDeals=result1[0].deals;
                var driverOrderId=result1[0].last_order_id;
                var driverTotalDeals=result1[0].total_deals;
                driverTotalDeals +=1;
                var dateOfdelivered = new Date();
                if(driverDeals==6){
                    var sql = "SELECT `customer_id` FROM `tb_order` WHERE `id`=? LIMIT 1";
                    dbConnection.Query(res, sql, [driverOrderId], function (result11) {
                        if (result11.length == 0) {
                            sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.WRONG_IN_ORDER_TB, res);
                        }
                        else {
                            var clientId=result11[0].customer_id;
                            var sql = "SELECT `total_deals` FROM `tb_user` WHERE `id`=? LIMIT 1";
                            dbConnection.Query(res, sql, [clientId,driverOrderId], function (result111) {
                                if (result111.length == 0) {
                                    sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.NO_CLIENT_AVAILABLE, res);
                                }
                                else {
                                    var userTotalDeals=result111[0].total_deals;
                                    userTotalDeals +=1;
                                    var sql="UPDATE `tb_user` SET `deals`=?,`total_deals`=? WHERE `id`=?";
                                    dbConnection.Query(res,sql, [1,driverTotalDeals,driverId] , function (userResponse3){
                                        var sql="UPDATE `tb_order` SET `status`=?,`delivered_time`=?,`delivered_location_address`=?,`delivered_latitude`=?,`delivered _longitude`=?,`price`=? WHERE `id`=? ";
                                        dbConnection.Query(res,sql, [3,dateOfdelivered,deliveredAddress,deliveredLatitude,deliveredLongitude,deliveredPrice,driverOrderId] , function (userResponse3){
                                            var sql="UPDATE `tb_user` SET `deals`=?,`total_deals`=? WHERE `id`=?";
                                            dbConnection.Query(res,sql, [1,userTotalDeals,clientId] , function (userResponse3) {
                                                //send notification to user and also driver for start service
                                                var data={
                                                    riding_price:deliveredPrice,
                                                    total_deals:driverTotalDeals
                                                };
                                                sendResponse.sendSuccessData(data, res);
                                            });
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
                else{
                    sendResponse.sendMsgWithStatusCode(constant.responseStatus.SHOW_ERROR_MESSAGE, constant.responseMessage.ORDERED_NOT_ACCEPTED, res);
                }

            }
        });
    });

});


/* get order api use to get driver whose are available. */
router.post('/get_driver', function (req, res) {
    var userAccessToken = req.body.access_token;
    var userLatitude = req.body.latitude;
    var userLongitude = req.body.longitude;
    var radiusFromUser = req.body.km;
    var checkVal = [userAccessToken, userLatitude, userLongitude, radiusFromUser];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkVal, callback);
        }, function (callback) {
            getDrivers(res, userAccessToken, userLatitude, userLongitude, radiusFromUser, callback);
        }], function (resultCallback) {
        res.send(resultCallback);
    });
});
function getDrivers(res, userAccessToken, lat, long, radius, callback) {
    var existingCords = [];
    var radiusToMeter = radius * 1000;
    var latOfUser = lat;
    var longOfUser = long;
    var userAccessToken = userAccessToken;
    var sql = "SELECT `id` FROM `tb_user` WHERE `access_token`=? LIMIT 1";
    dbConnection.Query(res, sql, [userAccessToken], function (result1) {
        if (result1.length == 0) {
            sendResponse.sendMsgWithStatusCode(constant.responseStatus.INVALID_ACCESS_TOKEN,constant.responseMessage.INVALID_ACCESS_TOKEN,res);
        }
        else {
            var sql = "SELECT `id`,`name`,`phone_no`,`current_latitude` as latitude,`current_longitude` as longitude FROM `tb_user` WHERE `status`=? AND `verified`=? and `deals`=?";
            dbConnection.Query(res, sql, [1, 1, 1], function (result) {
                if (result1.length == 0) {
                    sendResponse.sendErrorMessage(constant.responseMessage.DRIVER_NOT_AVAILABLE, res);
                }
                else {
                    var lengthOfResult = result.length;
                    //console.log(lengthOfResult);
                    for (var i = 0; i < lengthOfResult; i++) {
                        if (geoLib.isPointInCircle(
                                {latitude: result[i].latitude, longitude: result[i].longitude},
                                {latitude: latOfUser, longitude: longOfUser},
                                radiusToMeter
                            )) {
                            var sortedOfExistingCords = [];

                            sortedOfExistingCords = geoLib.orderByDistance({
                                latitude: latOfUser,
                                longitude: longOfUser
                            }, [
                                {latitude: result[i].latitude, longitude: result[i].longitude}
                            ]);
                            //existingCords={id:result[i].id,name:result[i].name,phone_no:result[i].phone_no,latitude:result[i].latitude,longitude:result[i].longitude,distance:sortedOfExistingCords[0].distance};
                            existingCords.push({
                                id: result[i].id,
                                name: result[i].name,
                                phone_no: result[i].phone_no,
                                latitude: result[i].latitude,
                                longitude: result[i].longitude,
                                distance: sortedOfExistingCords[0].distance
                            })
                        }
                    }
                    existingCords = commonFunc.sortByKey(existingCords, 'distance')
                    //console.log(existingCords);
                    return callback(existingCords);
                }
            });
        }
    });

}


module.exports = router;