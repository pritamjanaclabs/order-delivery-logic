/**
 * Created by karan on 3/13/2015.
 */
var express = require('express');
var router = express.Router();
var commonFunc = require('./commonFunction');
var async = require('async');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');

router.post('/add_driver', function (req, res) {
    var driverName = req.body.driver_name;
    var driverEmail=req.body.driver_email;
    var driverPhone=req.body.driver_phone_no;
    var driverPassword=req.body.driver_password;
    var driverDeviceToken=req.body.device_token;
    var driverDeviceType=req.body.device_type;
    var driverAddress=req.body.driver_address;
    var driverCurrentLatitude = req.body.drop_latitude;
    var driverCurrentLongitude = req.body.drop_longitude;
    var driverImage=req.files.driver_image;
    var checkValues = [driverName,driverEmail,driverPhone,driverPassword,driverDeviceToken,driverDeviceType,driverAddress, driverCurrentLatitude, driverCurrentLongitude,driverImage.name];
    var currentTime = new Date().getTime().toString();
    req.files.driver_image.name = currentTime;
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, checkValues, callback);
        },function(callback){
            commonFunc.checkEmailAvailability(res, driverEmail, callback);
        },function (callback){
            commonFunc.checkValidMailOnSMTP(res,driverEmail,callback);
        }], function (err, resultCallback) {
        commonFunc.imageUploadOnServer(req.files.driver_image, 'pjImages', function (imgLink) {
            if (imgLink === 0) {
                console.log("error");
                sendResponse.sendErrorMessage(constant.responseMessage.UPLOADING_ERROR, res);
            }
            else {
                var driverImage=imgLink;
                var loginTime = new Date();
                var accessToken = md5(driverEmail + driverPassword + loginTime);
                var encryptPassword = md5(driverPassword);
                var appVersion=100;     //initial app version
                var regAs=1;            //0: customer, 1: both
                var status=1;           //0: offline, 1: online as a driver, 2: online as a customer
                var verified=0;         //0: not, 1: yes
                var deals=1             //initially 0
                var oneTimeLink=0       //initially 0
                var sql = "INSERT INTO `tb_user`(`name`,`email`,`phone_no`,`password`,`address`, `current_latitude`, `current_longitude`,`date_registered`,`last_login`,`user_image`,`device_token`,`access_token`,`device_type`,`app_version`, `reg_as`,`status`,`verified`,`deals`,`one_time_link`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                var values = [driverName, driverEmail,driverPhone,encryptPassword,driverAddress, driverCurrentLatitude, driverCurrentLongitude,loginTime,loginTime,driverImage,driverDeviceToken,accessToken,driverDeviceType,appVersion, regAs,status,verified,deals,oneTimeLink];
                dbConnection.Query(res, sql, values, function (driverInsertResult) {
                    var data = {"driver_name": driverName, "driver_image": driverImage, "access_token": accessToken,
                        "email": driverEmail, "phone_no": driverPhone};
                    sendResponse.sendSuccessData(data, res);
                });
            }
        });
        //res.send("valid");
    });
    //console.log(req.body.driver_name);
    //res.send(req.body.driver_name);
});

module.exports = router;

